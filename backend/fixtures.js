const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require('./models/User');
const Cocktail = require('./models/Cocktail');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [firstUser, secondUser] = await User.create({
        email: 'admin@gmail.com',
        password: '123',
        token: nanoid(),
        displayName: 'Admin',
        avatar: 'fixtures/admin.jpg',
        role: 'admin'
    }, {
        email: 'user@gmail.com',
        password: '123',
        token: nanoid(),
        displayName: 'User',
        avatar: 'fixtures/user.jpeg',
        role: 'user',
    });

    await Cocktail.create({
            user: firstUser,
            name: 'High Horse',
            image: 'fixtures/highHorse.webp',
            recipe: 'Shannon Tebay, currently heading up the American Bar in London, ' +
                'created this cocktail while at NYC’s Death & Co. Calling it a rum Manhattan wouldn’t be ' +
                'far off; in a nod to Colonial-era ingredients, it uses aged rum, brandy, cherry liqueur, ' +
                'sweet vermouth, and Angostura bitters. ',
            published: true,
            ingredients: [
                {title: 'First Ingredient', amount: '20ml'},
                {title: 'Second Ingredient', amount: '10ml'},
                {title: 'Third Ingredient', amount: '40ml'},
                {title: 'Fourth Ingredient', amount: '50ml'},
            ]
        }, {
            user: firstUser,
            name: 'One Last Midnight',
            image: 'fixtures/lastMidnight.webp',
            recipe: 'Another cocktail created by Dorman, this drink calls for aged Venezuelan rum and two Italian vermouths, ' +
                'plus a pinch of coffee-infused salt and a spritz of ' +
                'Islay scotch to finish. As its name implies, it makes an ideal nightcap.',
            published: false,
            ingredients: [
                {title: 'First Ingredient', amount: '30ml'},
                {title: 'Second Ingredient', amount: '10ml'},
                {title: 'Third Ingredient', amount: '20ml'},
                {title: 'Fourth Ingredient', amount: '70ml'},
            ]
        }, {
            user: firstUser,
            name: 'Undead Gentleman',
            image: 'fixtures/undead.webp',
            recipe: 'Somewhere between the classic tropical drinks Zombie and Jet Pilot lies this Tiki drink from Martin Cate of Smuggler’s ' +
                'Cove in San Francisco. Two different rums, grapefruit and lime juices, falernum, cinnamon syrup, and Angostura bitters get ' +
                'mixed and strained into an absinthe-rinsed glass. The eight ingredients—before you even get to the garnishes—may be more than ' +
                'many cocktails call for, but the resulting drink is worth the extra effort.',
            published: false,
            ingredients: [
                {title: 'First Ingredient', amount: '30ml'},
                {title: 'Second Ingredient', amount: '10ml'},
                {title: 'Third Ingredient', amount: '20ml'},
                {title: 'Fourth Ingredient', amount: '70ml'},
            ]
        }
        , {
            user: secondUser,
            name: 'Daiquiri',
            image: 'fixtures/daiquiri.webp',
            recipe: 'Shannon Tebay, currently heading up the American Bar in London, ' +
                'created this cocktail while at NYC’s Death & Co. Calling it a rum Manhattan wouldn’t be ' +
                'far off; in a nod to Colonial-era ingredients, it uses aged rum, brandy, cherry liqueur, ' +
                'sweet vermouth, and Angostura bitters. ',
            published: true,
            ingredients: [
                {title: 'First Ingredient', amount: '20ml'},
                {title: 'Second Ingredient', amount: '30ml'},
                {title: 'Third Ingredient', amount: '10ml'},
                {title: 'Fourth Ingredient', amount: '90ml'},
            ]
        }, {
            user: secondUser,
            name: 'Painkiller',
            image: 'fixtures/painKiller.webp',
            recipe: 'Shannon Tebay, currently heading up the American Bar in London, ' +
                'created this cocktail while at NYC’s Death & Co. Calling it a rum Manhattan wouldn’t be ' +
                'far off; in a nod to Colonial-era ingredients, it uses aged rum, brandy, cherry liqueur, ' +
                'sweet vermouth, and Angostura bitters. ',
            published: false,
            ingredients: [
                {title: 'First Ingredient', amount: '50ml'},
                {title: 'Second Ingredient', amount: '20ml'},
                {title: 'Third Ingredient', amount: '70ml'},
                {title: 'Fourth Ingredient', amount: '100ml'},
            ],
        }, {
            user: secondUser,
            name: 'White Bat',
            image: 'fixtures/whiteBat.webp',
            recipe: 'Bartending vet Simon Ford came up with this drink somewhere between a White Russian and a Rum & Coke. ' +
                'A mixture of rum, Kahlua, and whole milk gets topped with cola and garnished with mint leaves for a deliciously ' +
                'sweet guilty-pleasure drink.',
            published: true,
            ingredients: [
                {title: 'First Ingredient', amount: '30ml'},
                {title: 'Second Ingredient', amount: '100m'},
                {title: 'Third Ingredient', amount: '40ml'},
                {title: 'Fourth Ingredient', amount: '200ml'},
            ],
        });

    await mongoose.connection.close();
};

run().catch(e => console.error(e));
