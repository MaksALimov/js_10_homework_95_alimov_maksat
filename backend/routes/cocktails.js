const express = require('express');
const multer = require('multer');
const Cocktail = require('../models/Cocktail');
const config = require('../config');
const {nanoid} = require('nanoid');
const path = require('path');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const router = express.Router();
const upload = multer({storage});

router.get('/', auth, async (req, res) => {
    try {
        const allCocktails = await Cocktail.find().select('name image published');
        const publishedCocktails = allCocktails.filter(cocktail => cocktail.published === true);

        if (req.query.userId) {
            const userCocktails = await Cocktail.find({user: req.query.userId});
            return res.send(userCocktails);
        }

        if (req.user.role === 'user') {
            return res.send(publishedCocktails);
        }

        res.send(allCocktails);

    } catch (error) {
        res.sendStatus(500);
    }
});

router.get('/:id', auth, async (req, res) => {
    try {
        const cocktail = await Cocktail.findById(req.params.id);

        if (!cocktail) {
            return res.status(404).send({error: 'Not Found'});
        }

        res.send(cocktail);

    } catch (error) {
        res.sendStatus(500);
    }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const ingredients = [];

        const ingTitle = [req.body.title].flat(1);
        const ingAmount = [req.body.amount].flat(1);

        for (let i = 0; i < ingTitle.length; i++) {
            ingredients.push({title: ingTitle[i], amount: ingAmount[i]});
        }

        const cocktailData = {
            user: req.user._id,
            name: req.body.name,
            recipe: req.body.recipe || null,
            ingredients,
        };

        if (req.file) {
            cocktailData.image = 'uploads/' + req.file.filename
        }

        const cocktail = new Cocktail(cocktailData);
        await cocktail.save();

        res.send(JSON.stringify(cocktail));
    } catch (error) {
        res.status(400).send(error);
    }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const cocktail = await Cocktail.findById(req.params.id);

        if (!cocktail) {
            return res.status(404).send({error: 'Cocktail Not Found'});
        }

        cocktail.published = !cocktail.published;

        await cocktail.save();
        return res.send(cocktail);
    } catch (error) {
        res.sendStatus(500);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
   try {
        const cocktail = await Cocktail.findByIdAndDelete(req.params.id);

        if (!cocktail) {
            return res.status(404).send({error: 'Cocktail Not Found'});
        }

        res.send(cocktail);
   } catch (error) {
        res.sendStatus(500);
   }
});

module.exports = router;