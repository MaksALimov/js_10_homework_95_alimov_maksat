import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    cocktails: [],
    addCocktailLoading: false,
    addCocktailError: null,
    getCocktailsLoading: false,
    getCocktailsError: null,
    getOneCocktailLoading: false,
    getOneCocktailError: null,
    specificCocktail: null,
    myCocktailsLoading: false,
    myCocktailsError: null,
    publishCocktailLoading: false,
    publishCocktailError: null,
    deleteCocktailLoading: false,
    deleteCocktailError: null,
};

const cocktailsSlice = createSlice({
    name: 'cocktails',
    initialState,

    reducers: {
        addCocktailRequest(state) {
            state.addCocktailLoading = true;
        },

        addCocktailSuccess(state) {
            state.addCocktailLoading = false;
        },

        addCocktailFailure(state, {payload: error}) {
            state.addCocktailLoading = false;
            state.addCocktailError = error;
        },

        getCocktailsRequest(state) {
            state.getCocktailsLoading = true;
            state.getCocktailsError = false;
        },

        getCocktailsSuccess(state, {payload: cocktails}) {
            state.getCocktailsLoading = false;
            state.cocktails = cocktails;
        },

        getCocktailsFailure(state, {payload: error}) {
            state.getCocktailsFailure = error;
            state.getCocktailsLoading = false;
        },

        getOneCocktailRequest(state) {
            state.getOneCocktailLoading = true;
        },

        getOneCocktailSuccess(state, action) {
            state.specificCocktail = action.payload;
            state.getOneCocktailLoading = false;
        },

        getOneCocktailFailure(state, {payload: error}) {
            state.getOneCocktailError = error;
            state.getOneCocktailLoading = false;
        },

        getMyCocktailsRequest(state) {
            state.myCocktailsLoading = true;
        },

        getMyCocktailsSuccess(state, {payload: usersCocktails}) {
            state.cocktails = usersCocktails;
            state.myCocktailsLoading = false;
        },

        getMyCocktailsFailure(state, {payload: error}) {
            state.myCocktailsLoading = false;
            state.myCocktailsError = error;
        },

        publishCocktailRequest(state) {
            state.publishCocktailLoading = true;
        },

        publishCocktailSuccess(state) {
            state.publishCocktailLoading = false;
        },

        publishCocktailFailure(state, {payload: error}) {
            state.publishCocktailLoading = false;
            state.publishCocktailError = error;
        },

        deleteCocktailRequest(state) {
            state.deleteCocktailLoading = true;
        },

        deleteCocktailSuccess(state, {payload: cocktailId}) {
            state.deleteCocktailLoading = false;
            state.cocktails = state.cocktails.filter(cocktail => cocktail._id !== cocktailId);
        },

        deleteCocktailFailure(state, {payload: error}) {
            state.deleteCocktailLoading = false;
            state.deleteCocktailError = error;
        }
    },
});

export default cocktailsSlice;