import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    user: null,
    loginError: null,
    loginLoading: null,
    logoutError: null,
    logoutLoading: null,
};

const userSlice = createSlice({
    name: 'users',
    initialState,

    reducers: {
        loginUserRequest(state) {
            state.loginLoading = true;
        },

        loginUserSuccess(state, {payload: userData}) {
            state.loginError = null;
            state.user = userData;
            state.loginLoading = false;
        },

        loginUserFailure(state, {payload: error}) {
            state.loginError = error;
            state.loginLoading = false;
        },

        loginGoogleRequest(state) {
            return state;
        },

        logoutUserRequest(state) {
            state.user = null;
            state.logoutLoading = true;
        },

        logoutUserFailure(state, {payload: error}) {
            state.logoutError = error;
            state.logoutLoading = false;
        },

        clearTextFieldsErrors(state) {
            state.registerError = null;
            state.loginError = null;
        }
    },
});

export default userSlice;