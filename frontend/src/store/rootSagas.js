import {all} from 'redux-saga/effects'
import usersSaga from "./sagas/usersSagas";
import cocktailsSaga from "./sagas/cocktailsSaga";

export function* rootSagas() {
    yield all([
        ...usersSaga,
        ...cocktailsSaga,
    ]);
}