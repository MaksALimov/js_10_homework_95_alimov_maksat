import cocktailsSlice from "../slices/cocktailsSlice";

export const {
    addCocktailRequest,
    addCocktailSuccess,
    addCocktailFailure,
    getCocktailsRequest,
    getCocktailsSuccess,
    getCocktailsFailure,
    getOneCocktailRequest,
    getOneCocktailSuccess,
    getOneCocktailFailure,
    getMyCocktailsRequest,
    getMyCocktailsSuccess,
    getMyCocktailsFailure,
    publishCocktailRequest,
    publishCocktailSuccess,
    publishCocktailFailure,
    deleteCocktailRequest,
    deleteCocktailSuccess,
    deleteCocktailFailure,
} = cocktailsSlice.actions;