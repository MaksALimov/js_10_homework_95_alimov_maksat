import userSlice from "../slices/usersSlice";

export const {
    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,
    loginGoogleRequest,
    logoutUserRequest,
    logoutUserFailure,
    clearTextFieldsErrors,
} = userSlice.actions;