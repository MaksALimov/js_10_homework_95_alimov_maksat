import {takeEvery, put} from 'redux-saga/effects';
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {
    loginGoogleRequest,
    loginUserFailure,
    loginUserRequest,
    loginUserSuccess,
    logoutUserFailure, logoutUserRequest
} from "../actions/usersActions";
import {historyPush} from "../actions/historyActions";

function* loginGoogleSaga({payload: googleData}) {
    try {
        const response = yield axiosApi.post('/users/googleLogin', {
            tokenId: googleData.tokenId,
            googleId: googleData.googleId,
        });
        yield put(loginUserSuccess(response.data));
        toast.success('Login successful!');
        historyPush('/');
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

function* loginUserSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users/sessions', userData);
        yield put(loginUserSuccess(response.data));
        toast.success('Login successful!');
        historyPush('/');
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

function* logoutUser() {
    try {
        yield axiosApi.delete('/users/sessions');
    } catch (error) {
        yield put(logoutUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

const usersSaga = [
    takeEvery(loginUserRequest, loginUserSaga),
    takeEvery(loginGoogleRequest, loginGoogleSaga),
    takeEvery(logoutUserRequest, logoutUser),
];

export default usersSaga;