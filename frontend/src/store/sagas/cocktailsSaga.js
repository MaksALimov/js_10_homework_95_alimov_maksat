import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";
import {
    addCocktailFailure,
    addCocktailRequest,
    addCocktailSuccess,
    deleteCocktailFailure,
    deleteCocktailRequest,
    deleteCocktailSuccess,
    getCocktailsFailure,
    getCocktailsRequest,
    getCocktailsSuccess,
    getMyCocktailsFailure,
    getMyCocktailsRequest,
    getMyCocktailsSuccess,
    getOneCocktailFailure,
    getOneCocktailRequest,
    getOneCocktailSuccess,
    publishCocktailFailure,
    publishCocktailRequest,
    publishCocktailSuccess
} from '../actions/cocktailsActions'

function* addCocktailSaga({payload: data}) {
    try {
        yield axiosApi.post('/cocktails', data.formData);
        yield put(addCocktailSuccess());
        if (data.user.role === 'admin') {
            toast.success('You successfully added cocktail');
        } else {
            toast.success('Your cocktail is under review by the moderator');
        }
        historyPush('/');

    } catch (error) {
        yield put(addCocktailFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

function* getCocktailsSaga() {
    try {
        const response = yield axiosApi.get('/cocktails');
        yield put(getCocktailsSuccess(response.data));
    } catch (error) {
        yield put(getCocktailsFailure(error.response.data));
        toast.error(error.response.data.global || error.response.statusText);
    }
}

function* getSpecificCocktailSaga({payload: cocktailId}) {
    try {
        const response = yield axiosApi.get(`/cocktails/${cocktailId}`);
        yield put(getOneCocktailSuccess(response.data));
    } catch (error) {
        yield put(getOneCocktailFailure(error.response.data));
        toast.error(error.response.data.global || error.response.statusText);
    }
}

function* getMyCocktailsSaga({payload: userId}) {
    try {
        const response = yield axiosApi.get(`/cocktails?userId=${userId}`);
        yield put(getMyCocktailsSuccess(response.data));

    } catch (error) {
        yield put(getMyCocktailsFailure(error.response.data));
        toast.error(error.response.data.global || error.response.statusText);
    }
}

function* publishCocktailSaga({payload: cocktailId}) {
    try {
        yield axiosApi.put(`/cocktails/${cocktailId}`);
        yield put(publishCocktailSuccess());
        yield getCocktailsSaga();
    } catch (error) {
        yield put(publishCocktailFailure(error.response.data));
        toast.error(error.response.data.global || error.response.data.error || error.response.statusText);
    }
}

function* deleteCocktailSaga({payload: cocktailId}) {
    try {
        yield axiosApi.delete(`/cocktails/${cocktailId}`);
        yield put(deleteCocktailSuccess(cocktailId));
    } catch (error) {
        yield put(deleteCocktailFailure(error.response.data));
        toast.error(error.response.data.global || error.response.data.error || error.response.statusText);
    }
}

const cocktailsSaga = [
    takeEvery(addCocktailRequest, addCocktailSaga),
    takeEvery(getCocktailsRequest, getCocktailsSaga),
    takeEvery(getOneCocktailRequest, getSpecificCocktailSaga),
    takeEvery(getMyCocktailsRequest, getMyCocktailsSaga),
    takeEvery(publishCocktailRequest, publishCocktailSaga),
    takeEvery(deleteCocktailRequest, deleteCocktailSaga),
];

export default cocktailsSaga;