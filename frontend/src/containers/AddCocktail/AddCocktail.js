import React, {useState} from 'react';
import {Button, Container, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {addCocktailRequest} from "../../store/actions/cocktailsActions.js";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import IngredientFields from "../../components/IngredientFields/IngredientFields";

const useStyles = makeStyles(() => ({
    container: {
        marginTop: '180px',
    },
}));

const AddCocktail = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const error = useSelector(state => state.cocktails.addCocktailError);
    const loading = useSelector(state => state.cocktails.addCocktailLoading);

    const [ingredients, setIngredients] = useState([
        {title: '', amount: ''},
    ]);

    const [cocktail, setCocktail] = useState({
        name: '',
        recipe: '',
        image: null,
    })

    const changeIngredients = (idx, name, value) => {
        setIngredients(prevState => {
            const ingredientsCopy = {
                ...prevState[idx],
                [name]: value,
            };

            return prevState.map((ing, index) => {
                if (idx === index) {
                    return ingredientsCopy;
                }

                return ing;
            });
        });
    };

    const addIngredients = () => {
        setIngredients(prevState => [
            ...prevState,
            {title: '', amount: ''}
        ]);
    };

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setCocktail(prevState => ({...prevState, [name]: value}));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setCocktail(prevState => {
            return {...prevState, [name]: file}
        });
    };

    const submitFormHandler = e => {
        e.preventDefault();
        const united = [...ingredients, cocktail];

        const formData = new FormData();
        for (const key of united) {
            Object.keys(key).forEach(ing => {
                formData.append(ing, key[ing]);
            })
        }

        dispatch(addCocktailRequest({formData, user}))
    };

    const deleteIngredients = (idx) => {
        setIngredients(prevState => prevState.filter((ing, i) => i !== idx));
    };

    console.log(ingredients);
    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Container className={classes.container} maxWidth="lg">
            <Grid container direction="column" component="form" spacing={2} onSubmit={submitFormHandler}>
                <Grid item>
                    <Typography variant="h4">
                        Add new Cocktail
                    </Typography>
                </Grid>
                <FormElement
                    onChange={inputChangeHandler}
                    name="name"
                    label="Name"
                    value={cocktail.name}
                    error={getFieldError('name')}
                />
               <IngredientFields
                   deleteIngredients={deleteIngredients}
                   changeIngredients={changeIngredients}
                   ingredients={ingredients}
               />
                <Grid item xs={4}>
                    <Button variant="contained" onClick={addIngredients}>
                        Add ingredient
                    </Button>
                </Grid>
                <FormElement
                    onChange={inputChangeHandler}
                    name="recipe"
                    label="Recipe"
                    multiline
                    rows={3}
                    value={cocktail.recipe}
                    error={getFieldError('recipe')}
                />
                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        fullWidth
                        variant="outlined"
                        onChange={fileChangeHandler}
                    />
                </Grid>
                <Grid item>
                    <ButtonWithProgress
                        type="submit"
                        variant="contained"
                        loading={loading}
                        disabled={loading}
                    >
                        Create Cocktail
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </Container>
    );
};

export default AddCocktail;

