import React, {useEffect} from 'react';
import {CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getOneCocktailRequest} from "../../store/actions/cocktailsActions";
import imageNotAvailable from '../../assets/images/noImage.png';
import {apiUrl} from "../../config";
import Ingredient from "../../components/Ingredient/Ingredient";
import Recipe from "../../components/Recipe/Recipe";

const useStyles = makeStyles(() => ({
    container: {
        marginTop: '180px',
        marginBottom: '70px',
    },

    imageContainer: {
        width: '100%',
        height: '100%',
        boxShadow: 'rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px',
        padding: '10px',
        textAlign: 'center',
    },

    cocktailContainer: {
        boxShadow: 'rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px',
        padding: '10px 40px',
        textAlign: 'center',
    },

    image: {
        width: '100%',
        height: 'auto%',
    },

    ingTitle: {
        margin: '20px 0',
    },
}));

const SpecificCocktail = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cocktail = useSelector(state => state.cocktails.specificCocktail);
    const loading = useSelector(state => state.cocktails.getOneCocktailLoading);

    useEffect(() => {
        dispatch(getOneCocktailRequest(match.params.id))
    }, [dispatch, match.params.id]);

    let cocktailImage = imageNotAvailable

    if (cocktail?.image) {
        cocktailImage = apiUrl + '/' + cocktail.image;
    }

    return cocktail && (
        loading ? <Grid container justifyContent="center" className={classes.container}><CircularProgress/></Grid> : (
            <Grid>
                <Grid container justifyContent="space-around" className={classes.container}>
                    <Grid item xs={3} className={classes.imageContainer}>
                        <img src={cocktailImage} alt="cocktailImage" className={classes.image}/>
                    </Grid>
                    <Grid container item xs={2} direction="column" alignItems="center"
                          className={classes.cocktailContainer}>
                        <Typography variant="h2">
                            {cocktail.name}
                        </Typography>
                        <Typography variant="h4" className={classes.ingTitle}>
                            Ingredients
                        </Typography>
                        {cocktail.ingredients.map(ingredient => (
                            <Ingredient
                                key={ingredient._id}
                                id={ingredient._id}
                                amount={ingredient.amount}
                                title={ingredient.title}
                            />
                        ))}
                    </Grid>
                </Grid>
                <Recipe recipe={cocktail.recipe}/>
            </Grid>
        )
    );
};

export default SpecificCocktail;