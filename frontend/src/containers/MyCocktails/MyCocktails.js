import React, {useEffect} from 'react';
import {CircularProgress, Grid, makeStyles} from "@material-ui/core";
import {getMyCocktailsRequest} from "../../store/actions/cocktailsActions";
import {useDispatch, useSelector} from "react-redux";
import Cocktail from "../../components/Cocktail/Cocktail";

const useStyles = makeStyles(() => ({
    container: {
        marginTop: '160px',
    },
}));

const MyCocktails = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const loading = useSelector(state => state.cocktails.myCocktailsLoading);

    useEffect(() => {
        dispatch(getMyCocktailsRequest(user._id))
    }, [dispatch, user._id]);

    return (
        <div className={classes.container}>
            {loading ? <Grid container justifyContent="center"><CircularProgress/></Grid> : (
                cocktails.map(cocktail => (
                    <Cocktail
                        key={cocktail._id}
                        image={cocktail.image}
                        id={cocktail._id}
                        name={cocktail.name}
                    />
                ))
            )}
        </div>
    );
};

export default MyCocktails;