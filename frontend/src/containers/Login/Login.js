import React, {useEffect, useState} from 'react';
import {Avatar, Container, Grid, makeStyles, Typography} from "@material-ui/core";
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';
import FormElement from "../../components/UI/Form/FormElement";
import GoogleLogin from "../../components/GooleLogin/GooleLogin";
import {clearTextFieldsErrors, loginUserRequest} from "../../store/actions/usersActions";
import {useDispatch, useSelector} from "react-redux";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {Alert} from "@material-ui/lab";

const useStyles = makeStyles(theme => ({
    container: {
        marginTop: '50px',
    },

    paper: {
        marginTop: "150px",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },

    signUpInscription: {
        fontSize: '20px',
        fontWeight: 'bold',
    },

    alert: {
        marginTop: theme.spacing(3),
        width: '100%',
    },
}));

const Login = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading = useSelector(state => state.users.loginLoading);
    const error = useSelector(state => state.users.loginError);

    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUserRequest({...user}));
    };

    useEffect(() => {
        return () => {
          dispatch(clearTextFieldsErrors());
        };
    }, [dispatch]);

    return (
        <Container component="section" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOpenOutlinedIcon/>
                </Avatar>
                <Typography component="h2" variant="h6">
                    Sign in
                </Typography>
                {
                    error &&
                    <Alert severity="error" className={classes.alert}>
                        {error.message || error.global}
                    </Alert>
                }
                <Grid container
                      component="form"
                      noValidate
                      spacing={3}
                      className={classes.container}
                      direction="column"
                      onSubmit={submitFormHandler}
                >
                    <FormElement
                        type="email"
                        required
                        autoComplete="new-email"
                        label="Email"
                        name="email"
                        value={user.email}
                        onChange={inputChangeHandler}
                    />
                    <FormElement
                        type="password"
                        required
                        autoComplete="new-password"
                        label="Password"
                        name="password"
                        value={user.password}
                        onChange={inputChangeHandler}
                    />

                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            loading={loading}
                            disabled={loading}
                        >
                            Sign in
                        </ButtonWithProgress>
                    </Grid>
                    <Grid item xs={12}>
                        <GoogleLogin/>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Login;