import React, {useEffect} from 'react';
import {CircularProgress, Grid, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getCocktailsRequest} from "../../store/actions/cocktailsActions";
import Cocktail from "../../components/Cocktail/Cocktail";

const useStyles = makeStyles(() => ({
    container: {
        marginTop: '180px',
    },
}));

const Cocktails = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cocktails = useSelector(state => state.cocktails.cocktails);
    const loading = useSelector(state => state.cocktails.getCocktailsLoading);

    useEffect(() => {
        dispatch(getCocktailsRequest());
    }, [dispatch]);

    return (
        <div className={classes.container}>
            {loading ? (
                <Grid
                    container
                    justifyContent="center">
                    <CircularProgress/>
                </Grid>
            ) : (
                cocktails.map(cocktail => (
                    <Cocktail
                        key={cocktail._id}
                        id={cocktail._id}
                        image={cocktail.image}
                        name={cocktail.name}
                        isPublished={cocktail.published}
                    />
                ))
            )}
        </div>
    );
};

export default Cocktails;