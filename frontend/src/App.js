import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Login from "./containers/Login/Login";
import AddCocktail from "./containers/AddCocktail/AddCocktail";
import {useSelector} from "react-redux";
import Cocktails from "./containers/Cocktails/Cocktails";
import MyCocktails from "./containers/MyCocktails/MyCocktails";
import SpecificCocktail from "./containers/SpecificCocktail/SpecificCocktail";

const App = () => {
    const user = useSelector(state => state.users.user);

    const Permit = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Permit
                    isAllowed={user}
                    path="/"
                    exact
                    component={Cocktails}
                    redirectTo="/login"
                />
                <Permit
                    isAllowed={user}
                    path="/add-cocktail"
                    component={AddCocktail}
                    redirectTo="/login"
                />
                <Permit
                    isAllowed={user}
                    path="/cocktail/:id"
                    component={SpecificCocktail}
                    redirectTo="/login"
                />
                <Permit
                    isAllowed={user}
                    path="/my-cocktails"
                    component={MyCocktails}
                    redirectTo="/login"
                />
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    )
};

export default App;
