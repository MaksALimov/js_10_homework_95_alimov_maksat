import React from 'react';
import {Link as RouterLink} from "react-router-dom";
import Anonymous from "./Menu/Anonymous";
import {AppBar, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import UserMenu from "./Menu/UserMenu";
import {useSelector} from "react-redux";

const useStyles = makeStyles(() => ({
    container: {
        padding: '20px',
    },

    mainLink: {
        color: 'inherit',
        textDecoration: 'none',
        '&:hover': {
            color: 'inherit',
        },
    },
}));

const Header = () => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    return (
        <>
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container
                          justifyContent="space-between"
                          alignItems="center"
                          className={classes.container}
                    >
                        <Grid item>
                            <Typography variant="h3">
                                <RouterLink to="/" className={classes.mainLink}>
                                    Cocktail Builder
                                </RouterLink>
                            </Typography>
                        </Grid>
                        <Grid item>
                            {user ? <UserMenu/> : <Anonymous/>}
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        </>
    );
};

export default Header;