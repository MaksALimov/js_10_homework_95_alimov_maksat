import React from 'react';
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {apiUrl} from "../../../../config";
import {logoutUserRequest} from "../../../../store/actions/usersActions";
import {Link as RouterLink} from "react-router-dom";

const useStyles = makeStyles(() => ({
    displayName: {
        fontSize: '35px',
    },

    imageContainer: {
        maxWidth: '120px',
        height: '120px',
    },

    linksBtn: {
        color: 'inherit',
        textDecoration: 'none',
    },

    image: {
        width: '100%',
        height: '100%',
    }
}));

const UserMenu = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    let avatarImage = user?.avatar;

    if (/fixtures/g.test(user?.avatar)) {
        avatarImage = apiUrl + '/' + user?.avatar;
    }

    return (
        <Grid container spacing={6} alignItems="center">
            <Grid item className={classes.imageContainer}>
                <img src={avatarImage} className={classes.image} alt="userPhoto"/>
            </Grid>
            <Grid item>
                <Typography className={classes.displayName}>
                    Hello {user.displayName}
                </Typography>
            </Grid>
            <Grid item>
                <Button variant="contained">
                    <RouterLink to="/add-cocktail" className={classes.linksBtn}>
                        Add Cocktail
                    </RouterLink>
                </Button>
            </Grid>
            <Grid item>
                <Button variant="contained">
                    <RouterLink to="/my-cocktails" className={classes.linksBtn}>
                        My Cocktails
                    </RouterLink>
                </Button>
            </Grid>
            <Grid item>
                <Button variant="contained" onClick={() => dispatch(logoutUserRequest())}>
                    Logout
                </Button>
            </Grid>
        </Grid>
    );
};

export default UserMenu;