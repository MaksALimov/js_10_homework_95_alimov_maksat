import React from 'react';
import {Button, Grid, makeStyles} from "@material-ui/core";
import {Link as RouterLink} from "react-router-dom";

const useStyles = makeStyles(() => ({
    buttons: {
        margin: '0 15px',
        fontWeight: 'bold',
        fontSize: '16px',
    },

    btnLinks: {
        color: 'inherit',
        textDecoration: 'none',
    },
}));

const Anonymous = () => {
    const classes = useStyles();
    return (
        <Grid item>
            <Button variant="contained" className={classes.buttons}>
                <RouterLink to="/login" className={classes.btnLinks}>
                    Login / Register
                </RouterLink>
            </Button>
        </Grid>
    );
};

export default Anonymous;