import React from 'react';
import GoogleLoginButton from 'react-google-login';
import {Button} from "@material-ui/core";
import {googleClientId} from "../../config";
import {useDispatch} from "react-redux";
import {loginGoogleRequest} from "../../store/actions/usersActions";

const GoogleLogin = () => {
    const dispatch = useDispatch();

    const googleLoginHandler = response => {
        dispatch(loginGoogleRequest(response));
    };

    return (
        <GoogleLoginButton
            clientId={googleClientId}
            render={props => (
                <Button fullWidth variant="outlined" color="primary" onClick={props.onClick}>
                    Login with Google
                </Button>
            )}
            onSuccess={googleLoginHandler}
            cookiePolicy={'single_host_origin'}
        />
    );
};

export default GoogleLogin;