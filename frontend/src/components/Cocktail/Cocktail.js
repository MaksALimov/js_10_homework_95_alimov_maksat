import React from 'react';
import {apiUrl} from "../../config";
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import noImageAvailable from '../../assets/images/noImage.png';
import {Link as RouterLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteCocktailRequest, publishCocktailRequest} from "../../store/actions/cocktailsActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(() => ({
    container: {
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
        margin: '20px auto',
        padding: '40px 0',
        textAlign: 'center',
    },

    imageContainer: {
        width: '40%',
        height: '40%',
    },

    image: {
        width: '80%',
        height: '80%',
    },

    cocktailName: {
        fontSize: '45px',
        fontWeight: 'bold',
        textTransform: 'capitalize',
        color: 'inherit',
        textDecoration: 'none',
        borderBottom: '4px solid black',
    },
}));

const Cocktail = ({name, image, id, isPublished}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const loading = useSelector(state => state.cocktails.publishCocktailLoading);

    let cocktailImage = noImageAvailable;

    if (image) {
        cocktailImage = apiUrl + '/' + image;
    }

    return (
        <Grid item container justifyContent="space-around" alignItems="center" className={classes.container} xs={8}>
            <Grid item xs={3}>
                <Typography>
                    <RouterLink to={`/cocktail/${id}`} className={classes.cocktailName}>
                        {name}
                    </RouterLink>
                </Typography>
            </Grid>
            <Grid item className={classes.imageContainer} xs={5}>
                <img className={classes.image} src={cocktailImage} alt="cocktailImage"/>
            </Grid>
            {user.role === 'admin' ? (
                <>
                    <Grid item>
                        <ButtonWithProgress
                            variant="contained"
                            loading={loading}
                            disabled={loading}
                            onClick={() => dispatch(publishCocktailRequest(id))}
                        >
                            {isPublished ? (
                                <Typography>Unpublish</Typography>
                            ) : (
                                <Typography>Publish</Typography>
                            )}
                        </ButtonWithProgress>
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            onClick={() => dispatch(deleteCocktailRequest(id))}
                        >
                            Delete
                        </Button>
                    </Grid>
                </>
            ) : null}
        </Grid>
    );
};

export default Cocktail;