import React from 'react';
import {Grid, Typography} from "@material-ui/core";

const Recipe = ({recipe}) => {
    return (
        <Grid container direction="column" alignItems="center" spacing={3}>
            <Grid item>
                <Typography variant="h5">
                    Recipe
                </Typography>
            </Grid>
            <Grid item>
                <Typography>
                    {recipe}
                </Typography>
            </Grid>
        </Grid>
    );
};

export default Recipe;