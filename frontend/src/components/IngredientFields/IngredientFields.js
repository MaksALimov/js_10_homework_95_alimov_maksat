import React from 'react';
import {Button, Grid, makeStyles} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";

const useStyles = makeStyles((theme) => ({
    textField: {
        margin: theme.spacing(1),
    },
}));

const IngredientFields = ({ingredients, changeIngredients, deleteIngredients}) => {
    const classes = useStyles();

    return (
        <div>
            {ingredients.map((ingredient, idx) => (
                <Grid
                    key={idx}
                    container
                    direction="row"
                    justifyContent="space-between"
                    alignItems="center"
                    className={classes.textField}
                >
                    <Grid item xs={5}>
                        <FormElement
                            onChange={e => changeIngredients(idx, 'title', e.target.value)}
                            name="title"
                            label="Ingredient name"
                            required
                            value={ingredient.title}
                        />
                    </Grid>
                    <Grid item xs={5}>
                        <FormElement
                            onChange={e => changeIngredients(idx, 'amount', e.target.value)}
                            label="Amount"
                            name="amount"
                            required
                            value={ingredient.amount}
                        />
                    </Grid>
                    <Grid item>
                        <Button variant="contained" onClick={() => deleteIngredients(idx)}>Delete</Button>
                    </Grid>
                </Grid>
            ))}
        </div>
    );
};

export default IngredientFields;