import React from 'react';
import {Grid, Typography} from "@material-ui/core";

const Ingredient = ({id, title, amount}) => {
    return (
        <Grid key={id} container justifyContent="space-between" alignItems="center">
            <Grid item>
                <Typography>{title}</Typography>
            </Grid>
            <Grid item>
                <Typography>{amount}</Typography>
            </Grid>
        </Grid>
    );
};

export default Ingredient;